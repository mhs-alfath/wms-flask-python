from flask import Flask, render_template, json, request, session, redirect, url_for
from array import array
from livereload import Server
from auth import createUser, cekUser, UserSignout
from query_menu import availablemenu_access
from model.admin_group import admin_group_data
from helper.helper import session_menu

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\sGDF545sdsdsdDDDDDGFddfn\xec]sdsv344dfd/'

data_array = {'listmenu':'', 'limit_table':10, 'offset_table':0}
#return_datatable = {'draw':'', 'recordsTotal':0, 'recordsFiltered':0, 'data':{}}


@app.route("/")
def main():
	#return render_template('index.html')
	return redirect(url_for('home'))
	
@app.route('/showSignUp')
def showSignUp():
	return render_template('signup.html')
	
@app.route('/showSignIn')
def showSignIn():
	#full_name = session['full_name']
	if not session.get('full_name'):
		return render_template('signin.html')
	else:
		return redirect(url_for('home'))
	
@app.route('/signUp',methods=['POST'])
def signUp():
	 # read the posted values from the UI
	_name = request.form['inputName']
	_email = request.form['inputEmail']
	_password = request.form['inputPassword']
	
	data = createUser(_name, _email, _password)
	if len(data) == 0:
		return json.dumps({'message':'User created success !'})
	else:
		return json.dumps({'error':str(data[0])})
	

@app.route('/signIn',methods=['POST'])
def signIn():
	
	_email = request.form['inputEmail']
	_password = request.form['inputPassword']
	
	data = cekUser(_email, _password)
	
	if data == 'true':
		return json.dumps({'message':data})
		#return redirect(url_for('home'))
	else:
		#return redirect(url_for('showSignIn'))
		return json.dumps({'message':data})
	

@app.route('/SignOut')
def SignOut():
	UserSignout()
	return redirect(url_for('showSignIn'))

	
@app.route('/home')
def home():
	if not session.get('full_name'):
		return render_template('signin.html')
	else:
		admin_id = session.get('admin_id')
		admin_group_id = session.get('admin_group_id')
		data_array['listmenu'] = availablemenu_access(admin_id, admin_group_id)
		#data_array = json.dumps(data_array)
		#data_array = [{'message':'pesanku'}, {'message':'ketiga'}]
		return render_template('home.html', values=data_array)
		#return json.dumps({'admin_id':data_array})
	


@app.route('/admin_group/index')
def admin_group():
	#data_array = {'listmenu':'', 'limit_table':10, 'offset_table':0}
	if not session.get('full_name'):
		return render_template('signin.html')
	else:
		admin_id = session.get('admin_id')
		admin_group_id = session.get('admin_group_id')
		data_array['listmenu'] = availablemenu_access(admin_id, admin_group_id)

		return render_template('admin_group/index.html', values=data_array)


@app.route('/ajax_admin_group_data', methods=['POST'])
def ajax_admin_group_data():
	return_datatable = admin_group_data()
	return return_datatable


@app.route('/admin_group_edit/<int:admin_group_id>')
def admin_group_edit(admin_group_id):
	#data_array = {'listmenu':'', 'limit_table':10, 'offset_table':0}
	if not session.get('full_name'):
		return render_template('signin.html')
	else:
		#admin_id = session.get('admin_id')
		#admin_group_id = session.get('admin_group_id')
		#data_array['listmenu'] = availablemenu_access(admin_id, admin_group_id)

		data_array['listmenu'] = session_menu()

		return render_template('admin_group/edit.html', values=data_array)



if __name__ == "__main__":
	server = Server(app.wsgi_app)
	server.serve()