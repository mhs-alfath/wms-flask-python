from flask import Flask
from flaskext.mysql import MySQL


app = Flask(__name__)
mysql = MySQL()


def mysqlconnection():
	# MySQL configurations
	app.config['MYSQL_DATABASE_USER'] = 'root'
	app.config['MYSQL_DATABASE_PASSWORD'] = ''
	app.config['MYSQL_DATABASE_DB'] = 'erp_logistic_retail'
	app.config['MYSQL_DATABASE_HOST'] = 'localhost'
	app.config['MYSQL_DATABASE_PORT'] = 3306
	app.config['MYSQL_DATABASE_DEBUG'] = 1
	mysql.init_app(app)
	conn = mysql.connect()
	
	return conn