from flask import Flask, json, session, request
from mysqlcon import mysqlconnection
from helper.helper import array_datatable

conn = mysqlconnection()
cursor = conn.cursor()



def count_admin_group(date_1='', date_2='', admin_group=''):
	#date_1, date_2, admin_group
	count_admin_group = 0
	if not date_1:
		date_1 = '1970-01-01 00:00:00'

	if not date_2:
		date_2 = '2020-04-01 00:00:00'

	str_query = "SELECT COUNT(*) AS count_admin_group FROM m_admin_group WHERE 1=1"
	str_query = str_query + " AND create_date >= %s "
	str_query = str_query + " AND create_date <= %s "
	if admin_group :
		admin_group = admin_group.lower()
		str_query = str_query + " AND lower(admin_group) LIKE %s "
		cursor.execute(str_query, (date_1, date_2, admin_group))
	else:
		cursor.execute(str_query, (date_1, date_2))

	datarow = cursor.fetchall()
	for row in datarow:
		count_admin_group = row[0]

	return count_admin_group


def select_admin_group(limit=10, offset=0, order_column=0, order_direction='DESC', date_1='', date_2='', admin_group='', admin_group_id=0):
	#date_1, date_2, admin_group
	count_admin_group = 0
	if not date_1:
		date_1 = '1970-01-01 00:00:00'

	if not date_2:
		date_2 = '2020-04-01 00:00:00'


	str_query = "SELECT admin_group_id, admin_group, create_date FROM m_admin_group WHERE 1=1"
	str_query = str_query + " AND create_date >= %s "
	str_query = str_query + " AND create_date <= %s "
	admin_group = admin_group.lower()
	if admin_group:
		str_query = str_query + " AND lower(admin_group) LIKE '"+admin_group+"'"
	if admin_group_id:
		str_query = str_query + " AND admin_group_id = "+str(admin_group_id)+""

	str_query = str_query + " LIMIT "+str(offset)+","+str(limit)
	cursor.execute(str_query, (date_1, date_2))
	datarow = cursor.fetchall()
	data_m = [0]
	m = 0
	for row in datarow:
		edit = '<a href="/admin_group_edit/' + str(row[0]) +'" target="_blank" title="Ubah"><i class="fa fa-edit fa-2x"></i></a>';
		delete = '<a href="#" onClick="Delete('+str(row[0])+', '+row[1]+'); return false;" title="Hapus"><i class="fa fa-trash-o fa-2x"></i></a>';
		action = edit +' '+ delete,
		data_m.append(m)
		data_m[m] = {'action':action, 'admin_group':row[1], 'create_date':row[2]}
			
		m += 1

	del data_m[m]

	return data_m


def admin_group_data():
	return_datatable = array_datatable()

	if not session.get('full_name'):
		return json.dumps(return_datatable)
	else:
		date_1 = request.args['date_1']
		if date_1:
			date_1 = date_1 + " 00:00:00"

		date_2 = request.args['date_2']
		if date_2:
			date_2 = date_2 + " 23:59:59"

		admin_group = request.args['admin_group']
		draw = request.form['draw']
		limit = request.form['length']
		offset = request.form['start']
		order_column = request.form['order[0][column]']
		order_direction = request.form['order[0][dir]']

		return_datatable['draw'] = draw
		return_datatable['recordsTotal'] = count_admin_group()
		return_datatable['recordsFiltered'] = count_admin_group(date_1, date_2, admin_group)
		return_datatable['data'] = select_admin_group(limit, offset, order_column, order_direction, date_1, date_2, admin_group)
		#data = select_admin_group(limit, offset, order_column, order_direction, date_1, date_2, admin_group)

		return json.dumps(return_datatable)


def rowedit_admin_group_data(admin_group_id):
	return_data = {'admin_group':'', 'data':{}}

	data = select_admin_group(1, 0, 0, 'desc', '', '', '', admin_group_id)
	del data[0]['action']
	del data[0]['create_date']

	return_data['admin_group'] = data[0]['admin_group']
	return_data['data'] = data

	return return_data






