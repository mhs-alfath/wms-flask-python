from flask import Flask, json, session
from query_menu import availablemenu_access

return_datatable = {'draw':'', 'recordsTotal':0, 'recordsFiltered':0, 'data':{}}
#data_array = {'listmenu':'', 'limit_table':10, 'offset_table':0}

def array_datatable():
	return return_datatable

def session_menu():
	admin_id = session.get('admin_id')
	admin_group_id = session.get('admin_group_id')
	listmenu = availablemenu_access(admin_id, admin_group_id)
	return listmenu

