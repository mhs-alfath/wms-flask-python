from flask import Flask, json, session, redirect, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from mysqlcon import mysqlconnection

app = Flask(__name__)
#app.secret_key = b'_5#y2L"F4Q8z\sdsdddfn\xec]sdsv344dfd/'

conn = mysqlconnection()
cursor = conn.cursor()



def createUser(_name, _email, _password):
	_hashed_password = generate_password_hash(_password)
	cursor.callproc('sp_createUser', (_name,_email,_hashed_password) )
	data = cursor.fetchall()
	conn.commit()
	
	return data
	
def cekUser(_email, _password):
	#_hashed_password = generate_password_hash(_password)
	cursor.callproc('sp_cekUser', (_email,'') )
	data = cursor.fetchall()
	conn.commit()
	if data[0][0] > 0:
		
		pwdondb = data[0][4]
		
		session['admin_id'] = data[0][0]
		session['admin'] = data[0][1]
		session['full_name'] = data[0][2]
		session['admin_group_id'] = data[0][3]
		
		cekkpassword = check_password_hash(pwdondb, _password)
	else:
		cekkpassword = "false"
	
	return cekkpassword
	

def UserSignout():
	session.pop('admin_id', None)
	session.pop('admin', None)
	session.pop('full_name', None)
	session.pop('admin_group_id', None)

