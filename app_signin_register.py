from flask import Flask, render_template, json, request
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash
from livereload import Server

app = Flask(__name__)
mysql = MySQL()
 
# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '123456'
app.config['MYSQL_DATABASE_DB'] = 'kjntruck'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_PORT'] = 3307
mysql.init_app(app)
conn = mysql.connect()
cursor = conn.cursor()


@app.route("/")
def main():
	return render_template('index.html')
	
@app.route('/showSignUp')
def showSignUp():
	return render_template('signup.html')
	
@app.route('/signUp',methods=['POST'])
def signUp():
	 # read the posted values from the UI
	try:
		
		_name = request.form['inputName']
		_email = request.form['inputEmail']
		_password = request.form['inputPassword']
		# validate the received values
		if _name and _email and _password:
			_hashed_password = generate_password_hash(_password)
			cursor.callproc('sp_createUser', (_name,_email,_hashed_password) )
			data = cursor.fetchall()
			if len(data) is 0:
				conn.commit()
				return json.dumps({'message':'User created successfully !'})
			else:
				return json.dumps({'error':str(data[0])})
		else:
			return json.dumps({'html':'<span>Enter the required fields</span>'})
		
	except Exception as e:
		return json.dumps({'error':str(e)})
	#finally:
	#	cursor.close()
	#	conn.close()
	


if __name__ == "__main__":
	server = Server(app.wsgi_app)
	server.serve()